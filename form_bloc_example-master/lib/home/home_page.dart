import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/home_bloc.dart';
import 'bloc/home_bloc.dart';
import 'bloc/home_bloc.dart';
import 'bloc/home_bloc.dart';
import 'bloc/home_bloc.dart';
import 'bloc/home_bloc.dart';
import 'bloc/home_bloc.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);
  
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String _filterValue = 'Todos';
  dynamic _currentEvent = GetAllUsersEvent();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Users list"),
        actions: <Widget>[
          DropdownButton(
            value: _filterValue,
            items: [
              DropdownMenuItem(child: Text("Todos"), value: "Todos"),
              DropdownMenuItem(child: Text("Pares"), value: "Pares"),
              DropdownMenuItem(child: Text("Impares"), value: "Impares")
            ],
            onChanged: (value) {
              setState(() {
                _filterValue = value;
                switch(_filterValue){
                  case "Todos": {
                    _currentEvent = GetAllUsersEvent;
                  }
                  break;

                  case "Pares": {
                    _currentEvent = FilterUsersEvent(filterEven: true);
                  }
                  break;

                  case "Impares": {
                    _currentEvent = FilterUsersEvent(filterEven: false);
                  }
                  break;

                  default: {
                    _currentEvent = GetAllUsersEvent;
                  }
                  break;
                }
              });
            },
          )
        ],
      ),
      body: BlocProvider(
        create: (context) => HomeBloc()..add(_currentEvent),
        
        child: BlocConsumer<HomeBloc, HomeState>(
          listener: (context, state) {
            // para mostrar dialogos o snackbars
            if (state is ErrorState) {
              Scaffold.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(content: Text("Error: ${state.error}")),
                );
            }
          },
          builder: (context, state) {
            if (state is ShowUsersState) {
              return RefreshIndicator(
                child: ListView.builder(
                  itemCount: state.usersList.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                      title: Text(state.usersList[index].name),
                    );
                  },
                ),
                onRefresh: () async {
                  BlocProvider.of<HomeBloc>(context).add( _currentEvent);
                },
              );
            } else if (state is LoadingState) {
              return Center(child: CircularProgressIndicator());
            }
            return Center(
              child: MaterialButton(
                onPressed: () {
                  BlocProvider.of<HomeBloc>(context).add( _currentEvent);
                },
                child: Text("Cargar de nuevo"),
              ),
            );
          },
        ),
      ),
    );
  }
}
